<?php

/**
 *  Implements hook_views_data().
 */
function artsci_worldpay_webforms_views_data() {
  $data['artsci_worldpay_webforms_postback']['table']['group'] = t('Worldpay');
  $data['artsci_worldpay_webforms_postback']['table']['join'] = array(
    'webform_submissions' => array(
      'left_field' => 'sid',
      'field' => 'sid',
    ),
  );

  $data['artsci_worldpay_webforms_postback']['payment_status'] = array(
    'title' => t('Payment status'),
    'help' => t('Status of Worldpay payment for this webform submission.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['artsci_worldpay_webforms_postback']['timestamp'] = array(
    'title' => t('Timestamp'),
    'help' => t('Timestamp when payment was received.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );

  $idFields = array(
    'orderid' => 'Order ID',
    'historyid' => 'History ID',
    'transid' => 'Transaction ID',
  );

  foreach ($idFields as $k => $v) {
    $data['artsci_worldpay_webforms_postback'][$k] = array(
      'title' => t($v),
      'field' => array(
        'handler' => 'views_handler_field',
        'click sortable' => TRUE,
      ),
      'filter' => array(
        'handler' => 'views_handler_filter_string',
      ),
      'sort' => array(
        'handler' => 'views_handler_sort',
      ),
    );
  }

  return $data;
}

