<?php

//utility function to convert key for use by PHP's mycrypt library
function hexToStr($hex){
 $string = '';
 for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
 $string .= chr(hexdec($hex[$i] . $hex[$i + 1]));
 }
 return $string;
}

function artsci_worldpay_decrypt($frisk_key, $encrypted_string) {
  $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_3DES, MCRYPT_MODE_ECB), MCRYPT_RAND);
  $decoded = trim(mcrypt_decrypt(MCRYPT_3DES, hexToStr($frisk_key), hexToStr($encrypted_string), MCRYPT_MODE_ECB, $iv));
  return $decoded;
}

function artsci_worldpay_encrypt($frisk_key, $string) {
  $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_3DES, MCRYPT_MODE_ECB), MCRYPT_RAND);
  return mcrypt_encrypt(MCRYPT_3DES, hexToStr($frisk_key), $string, MCRYPT_MODE_ECB, $iv);
}

function artsci_worldpay_webform_generate_url($ip_account_id, $ip_sub_account_id, $ip_form_id, $ip_session_id, $frisk, $amount, $custom_data) {
  /* decrypt formid to get plain-text string */
  // create initialization vector
  $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_3DES, MCRYPT_MODE_ECB), MCRYPT_RAND);
  $decoded = trim(mcrypt_decrypt(MCRYPT_3DES, hexToStr($frisk), hexToStr($ip_form_id), MCRYPT_MODE_ECB, $iv));
  $decoded_parts = explode(':', $decoded);
  $ip_template_id = $decoded_parts[2]; // WebPay Form ID, i.e. '1000-123456789'
  /* now rebuild URL */
  // create initialization vector
  $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_3DES, MCRYPT_MODE_ECB), MCRYPT_RAND);
  // string to encrpyt, colon (:) deliminated
  $string = $ip_account_id.':'.$ip_sub_account_id.':'.$ip_template_id.':'.$amount.':';
  // encrypt string
  $encoded = mcrypt_encrypt(MCRYPT_3DES, hexToStr($frisk), $string, MCRYPT_MODE_ECB, $iv);
  $webpay_url = 'https://trans.worldpay.us/cgi-bin/WebPAY.cgi?formid=';
  $webpay_url .= strtoupper(bin2Hex($encoded));
  $webpay_url .= '&sessionid=';
  $webpay_url .= $ip_session_id;
  $webpay_url .= '&customdata=';
  $webpay_url .= strtoupper(bin2Hex(mcrypt_encrypt(MCRYPT_3DES, hexToStr($frisk), $custom_data, MCRYPT_MODE_ECB, $iv)));

  return $webpay_url;
}

