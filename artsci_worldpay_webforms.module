<?php

/**
 *  Implements hook_menu().
 */
function artsci_worldpay_webforms_menu() {
  // Node e-mail forms.
  $items['node/%webform_menu/webform/worldpay'] = array(
    'title' => 'Worldpay',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('artsci_worldpay_webforms_form', 1),
    'access callback' => 'node_access',
    'access arguments' => array('update', 1),
    'weight' => 10,
    'type' => MENU_LOCAL_TASK,
  );

  // TODO: screen in between submission and confirmation, with link to payment gateway
  /*$items['node/%webform_menu/payment'] = array(
    'title' => 'Webform payment link',
    'page callback' => '_artsci_worldpay_webforms_payment',
    'page arguments' => array(1),
    'access callback' => 'webform_confirmation_page_access',
    'access arguments' => array(1),
    'type' => MENU_CALLBACK,
  );*/

  $items['node/%/worldpay-postback-url'] = array(
    'title' => 'Worldpay Postback URL',
    'page callback' => '_artsci_worldpay_webforms_postback_url',
    'page arguments' => array(1),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

function _artsci_worldpay_webforms_postback_url($nid) {
  watchdog('hive', 'postback from worldpay - ' . print_r($_POST, true));

  $data = db_select('artsci_worldpay_webforms','w')
    ->fields('w', array('data'))
    ->condition('nid', $nid)
    ->execute()
    ->fetchField();

  if (!($data)) {
    return;
  }

  $data = unserialize($data);

  module_load_include('inc', 'artsci_worldpay_webforms', 'artsci_worldpay_webforms.webpay_helpers');
  $custom_data = artsci_worldpay_decrypt($data['3des_key'], $_POST['customdata']);
  watchdog('hive', 'decrypted customdata - ' . print_r($custom_data, true));

  $custom_data = explode('&', $custom_data);
  $custom = array();
  foreach ($custom_data as $param) {
    $kv = explode('=', $param, 2);
    $custom[$kv[0]] = urldecode($kv[1]);
  }

  db_merge('artsci_worldpay_webforms_postback')
    ->key(array('sid' => $custom['sid']))
    ->fields(array(
      'timestamp' => time(),
      'payment_status' => $_POST['Status'],
      'orderid' => $_POST['orderid'],
      'historyid' => $_POST['historyid'],
      'transid' => $_POST['transid'],
    ))
    ->execute();
}

function artsci_worldpay_webforms_theme() {
  return array(
    'artsci_worldpay_webforms_select_table' => array(
      'render element' => 'element'
    )
  );
}

function theme_artsci_worldpay_webforms_select_table($vars) {
  $element = $vars['element'];

  $rows = array();
  foreach (element_children($element) as $key) {
    $rows[] = array(
      array('data' => $element[$key]['artsci_worldpay_element']),
      array('data' => render($element[$key]['custom']))
    );
  }

  $header = array(t('WebPay config'), t('Value'));
  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 *  Implements drupal_get_form callback.
 */
function artsci_worldpay_webforms_form($form, &$form_state, $node) {
  $data = db_select('artsci_worldpay_webforms','w')
    ->fields('w', array('data'))
    ->condition('nid', $node->nid)
    ->execute()
    ->fetchField();

  if ($data) {
    $data = unserialize($data);
  }

  $form = array('#node' => $node->nid);

  $header = array(
    'component' => t('Component'),
    'type' => t('Type'),
    'artsci_worldpay_element' => t('Worldpay Element'),
  );

  $form['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#description' => t('If checked, any submissions to this form will be automatically sent to the Worldpay WebPay system.'),
    '#default_value' => isset($data['enabled'])?$data['enabled']:0,
  );

  // UX hack: only make these fields required when there is no value already
  // stored.  Since they are password fields, the default value can't be used
  // to carry the value through.
  $account_id_required = !isset($data['account_id']);
  $frisk_required = !isset($data['3des_key']);

  $form['webpay_config'] = array(
    '#title' => t('WebPay Configuration'),
    '#type' => 'fieldset',
    '#tree' => TRUE,
    'account_id' => array(
      '#title' => t('Account ID'),
      '#description' =>
        $account_id_required
        ? t('An account ID is required to enable the WebPay integration.')
	: t('This field has already been stored in the database.  You can enter '
          . 'a new value to reset it.'),
      '#type' => 'password',
      '#required' => $account_id_required,
    ),
    'sub_account_id' => array(
      '#title' => t('Sub-account ID'),
      '#type' => 'password',
      '#default_value' => isset($data['sub_account_id'])?$data['sub_account_id']:'',
      '#description' =>
        isset($data['sub_account_id']) 
          ? t('This field has already been stored in the database.  You can enter '
            . 'a new value to reset it.')
          : t('Please provide a valid Sub Account ID if needed.'),
    ),
    'form_id' => array(
      '#title' => t('Form ID'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => isset($data['form_id'])?$data['form_id']:'',
    ),
    'session_id' => array(
      '#title' => t('Session ID'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => isset($data['session_id'])?$data['session_id']:'',
    ),
    '3des_key' => array(
      '#title' => t('3DES key from FRISK'),
      '#description' =>
        $frisk_required
        ? t('The 3DES encryption key from FRISK is required '
          . 'to send custom charge amounts and track payment status.')
	: t('This field has already been stored in the database.  You can enter '
          . 'a new value to reset it.'),
      '#type' => 'password',
      '#required' => $frisk_required,
    ),
  );

  $form['amount_component'] = array(
    '#title' => t('Amount'),
    '#description' => t('Webform component determining amount of money to charge in WebPay form.  Amount should be in 0.00 format. Do not include currency symbols or commas.  Available for display as ::AMOUNT:: in advanced form configuration.'),
    '#type' => 'select',
    '#options' => array_map(function($c) { return $c['name']; }, $node->webform['components']),
    '#default_value' => isset($data['amount_component'])?$data['amount_component']:0,
  );

  if (!isset($form_state['custom_data_count'])) {
    if (isset($data['webpay_custom_data'])) {
      $form_state['custom_data_count'] = count($data['webpay_custom_data']);
    } else {
      $form_state['custom_data_count'] = 0;
    }
  }

  $form['webpay_custom_data'] = array(
    '#title' => t('Custom Fields'),
    '#description' => t('You may select up to 5 fields to make available for display in the WebPay form.  These will be available using the tokens ::CDATA1::, ::CDATA2::, etc.'),
    '#type' => 'fieldset',
    '#tree' => TRUE,
  );

  if ($form_state['custom_data_count']) {
    for ($i = 1; $i <= $form_state['custom_data_count']; $i++) {
      $form['webpay_custom_data']['cdata' . $i] = array(
        '#title' => 'CDATA' . $i,
        '#type' => 'select',
	'#options' => array_map(function($c) { return $c['name']; }, $node->webform['components']),
	'#default_value' => isset($data['webpay_custom_data']['cdata' . $i])?$data['webpay_custom_data']['cdata' . $i]:0,
      );
    }
  }

  if ($form_state['custom_data_count'] < 5) {
    $form['webpay_custom_data']['add_custom_field'] = array(
      '#type' => 'submit',
      '#value' => t('Add custom field...'),
      '#submit' => array('artsci_worldpay_webforms_form_add_custom_field'),
    );
  }

  if ($form_state['custom_data_count'] >= 1) {
    $form['webpay_custom_data']['remove_last_field'] = array(
      '#type' => 'submit',
      '#value' => t('Remove last custom field'),
      '#submit' => array('artsci_worldpay_webforms_form_remove_last_custom_field'),
      '#limit_validation_errors' => array(),
    );
  }

  $form['instructions'] = array(
    '#type' => 'markup',
    '#markup' => "
Make sure your WebPay form uses the following settings:<br/>
Postback URL: " . url('node/' . $node->nid . '/worldpay-postback-url', array('absolute' => TRUE)) . "<br/>
");

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  return $form;
}

// TODO: this would be nicer as ajax
function artsci_worldpay_webforms_form_add_custom_field($form, &$form_state) {
  watchdog('hive', '$form - <pre>' . print_r($form, true) . '</pre><br/>$form_state - <pre>' . print_r($form_state, true) . '</pre>');
  $form_state['custom_data_count']++;
  $form_state['rebuild'] = TRUE;
}

function artsci_worldpay_webforms_form_remove_last_custom_field($form, &$form_state) {
  if ($form_state['custom_data_count'] >= 1) {
    $form_state['custom_data_count']--;
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Implements hook_form_alter().
 */
function artsci_worldpay_webforms_form_alter(&$form, &$form_state, $form_id) {
  if (strpos($form_id, 'webform_client_form_') !== FALSE) {
    $form['#validate'][] = 'artsci_worldpay_webforms_client_form_validate';
    $form['#submit'][] = 'artsci_worldpay_webforms_client_form_submit';
  }
}

/**
 * Implements hook_form_submit().
 */
function artsci_worldpay_webforms_form_submit($form, &$form_state) {
  $data = db_select('artsci_worldpay_webforms','w')
    ->fields('w', array('data'))
    ->condition('nid', $form['#node'])
    ->execute()
    ->fetchField();

  if ($data) {
    $data = unserialize($data);
  } else {
    $data = array(
      'account_id' => '',
      '3des_key' => ''
    );
  }

  $data = array(
    'enabled' => $form_state['values']['enabled'],
    'account_id' =>
      !empty($form_state['values']['webpay_config']['account_id'])
        ? $form_state['values']['webpay_config']['account_id'] : $data['account_id'],
    'sub_account_id' => !empty($form_state['values']['webpay_config']['sub_account_id'])
        ? $form_state['values']['webpay_config']['sub_account_id'] : $data['sub_account_id'],
    'form_id' => $form_state['values']['webpay_config']['form_id'],
    'session_id' => $form_state['values']['webpay_config']['session_id'],
    '3des_key' =>
      !empty($form_state['values']['webpay_config']['3des_key'])
        ? $form_state['values']['webpay_config']['3des_key'] : $data['3des_key'],
    'amount_component' => $form_state['values']['amount_component'],
  );

  $data['webpay_custom_data'] = $form_state['values']['webpay_custom_data'];
  unset($data['webpay_custom_data']['add_custom_field']);
  unset($data['webpay_custom_data']['remove_last_field']);

  switch(db_merge('artsci_worldpay_webforms')
    ->key(array('nid' => $form['#node']))
    ->fields(array('data' => serialize($data)))
    ->execute()) {
    case MergeQuery::STATUS_INSERT:
    case MergeQuery::STATUS_UPDATE:
      drupal_set_message(t('Saved configuration changes for Worldpay WebPay payment gateway.'));
      break;
    default:
      drupal_set_message(t('Failed to save changes to the database.'), 'error');
  }
}

function artsci_worldpay_webforms_client_form_validate($form, &$form_state) {
  /*dpm($form);
  dpm($form_state);*/

  $data = db_select('artsci_worldpay_webforms','w')
    ->fields('w', array('data'))
    ->condition('nid', $form['#node']->nid)
    ->execute()
    ->fetchField();

  if (!($data)) {
    return;
  }
  
  $data = unserialize($data);

  if (!$data['enabled']) {
    return;
  }
}

function artsci_worldpay_webforms_client_form_submit($form, &$form_state) {
  $data = db_select('artsci_worldpay_webforms','w')
    ->fields('w', array('data'))
    ->condition('nid', $form['#node']->nid)
    ->execute()
    ->fetchField();

  if (!($data)) {
    return;
  }
  
  $data = unserialize($data);

}

function artsci_worldpay_webforms_webform_submission_insert($node, $submission) {
  artsci_worldpay_webforms_redirect_to_webpay($node, $submission);
}

function artsci_worldpay_webforms_webform_submission_update($node, $submission) {
  artsci_worldpay_webforms_redirect_to_webpay($node, $submission);
}

function artsci_worldpay_webforms_redirect_to_webpay($node, $submission) {
  if ($submission->is_draft !== 0) {
    return;
  }

  $data = db_select('artsci_worldpay_webforms','w')
    ->fields('w', array('data'))
    ->condition('nid', $node->nid)
    ->execute()
    ->fetchField();

  if (!($data)) {
    return;
  }
  
  $data = unserialize($data);

  if (!$data['enabled']) {
    return;
  }

  module_load_include('inc', 'artsci_worldpay_webforms', 'artsci_worldpay_webforms.webpay_helpers');

  $amount = _artsci_worldpay_webforms_format_amount($submission->data[$data['amount_component']][0]);

  if ($amount != '0.00') {
    $custom_data = 'sid=' . $submission->sid;

    foreach ($data['webpay_custom_data'] as $cdata => $component_id) {
      $custom_data .= '&' . $cdata . '=' . urlencode($submission->data[$component_id][0]);
    }

    $webpay_url = artsci_worldpay_webform_generate_url($data['account_id'], $data['sub_account_id'], $data['form_id'], $data['session_id'], $data['3des_key'], $amount, $custom_data);

    drupal_goto($webpay_url, array('external' => TRUE));
  }
}

/**
 *  Make sure amount fields are formatted the way Worldpay expects: 0.00 format
 *
 *  Examples:
 *    $0.00 -> 0.00
 *    1,000 -> 1000.00
 *    100.050 -> 100.05
 */
function _artsci_worldpay_webforms_format_amount($amount) {
  $amount = floatval($amount);

  return number_format($amount, 2, '.', '');
}

/**
 * Implements hook_webform_submission_delete().
 */
function artsci_worldpay_webforms_webform_submission_delete($node, $submission) {
  db_delete('artsci_worldpay_webforms_postback')
    ->condition('sid', $submission->sid)
    ->execute();
}

/**
 * Implements hook_webform_submission_load().
 */
function artsci_worldpay_webforms_webform_submission_load(&$submissions) {
  $payments = db_select('artsci_worldpay_webforms_postback', 'p')
    ->fields('p')
    ->condition('sid', array_keys($submissions), 'IN')
    ->execute();

  foreach ($payments as $p) {
    $submissions[$p->sid]->payment_timestamp = $p->timestamp;
    foreach (array('payment_status', 'orderid', 'historyid', 'transid') as $k) {
      $submissions[$p->sid]->{$k} = $p->{$k};
    }
  }
}


/**
 *  Implements hook_webform_results_download_submission_information_info().
 */
function artsci_worldpay_webforms_webform_results_download_submission_information_info() {
  return array(
    'payment_status' => t('Worldpay Payment Status'),
    'payment_timestamp' => t('Worldpay Payment Timestamp'),
    'orderid' => t('Worldpay Order ID'),
    'historyid' => t('Worldpay History ID'),
    'transid' => t('Worldpay Transaction ID'),
  );
}

/**
 * Return values for submission data download fields.
 *
 * @param $token
 *   The name of the token being replaced.
 * @param $submission
 *   The data for an individual submission from webform_get_submissions().
 * @param $options
 *   A list of options that define the output format. These are generally passed
 *   through from the GUI interface.
 * @param $serial_start
 *   The starting position for the Serial column in the output.
 * @param $row_count
 *   The number of the row being generated.
 *
 * @return
 *   Value for requested submission information field.
 *
 * @see hook_webform_results_download_submission_information_info()
 */
function artsci_worldpay_webforms_webform_results_download_submission_information_data($token, $submission, array $options, $serial_start, $row_count) {
  switch ($token) {
    case 'payment_status':
    case 'payment_timestamp':
    case 'orderid':
    case 'historyid':
    case 'transid':
      return isset($submission->{$token})?$submission->{$token}:'';
  }
}


/**
 * Implements hook_views_api().
 */
function artsci_worldpay_webforms_views_api() {
  return array(
    'api' => 3.0,
  );
}

