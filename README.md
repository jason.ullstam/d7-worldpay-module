# artsci_worldpay_webforms

This module provides an integration between Drupal 7 Webforms and the Worldpay
WebPay payment gateway.  It’s intended to be a simpler solution than other
modules which rely on larger systems such as Commerce or UberCart.

The code is largely derived from the WPAY_WebPay_Integration_Guide.pdf document
available from WorldPay’s developer portal, which should be consulted for
further details.

## Installation

Clone repository into your modules directory:
```
$ cd /var/www/sites/all/modules
$ git clone https://github.com/UK-AS-HIVE/artsci_worldpay_webforms
```

This module requires the mcrypt PHP extension.

To install this on e.g. an Ubuntu server:
```
# apt-get install php5-mcrypt
# php5enmod mcrypt
```

Then, enable the module as usual:
```
$ drush en -y artsci_worldpay_webforms
```

## Usage

Once this module is enabled, a new secondary tab labeled ‘Worldpay’ will be
available under the Webform configuration for each webform node.  Some
coordination of setup will be required across both the Drupal Webform and the
WebPay form, so make sure both have been created.

### Configuring your Drupal webform (TODO)

### Configuring your WebPay webform (TODO)

## License

This module is currently intended only for internal use by the University of
Kentucky.  There is currently no license for external parties, until such time
as it is approved for release under the GPL by UK officials.  In other words,
keep it private!


